#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
int main()
{
    char fName[50], fData[100];
    int cf, of;
    printf("Enter the file name :: ");
    scanf("%s", fName);
    cf = creat(fName, __S_IREAD | __S_IWRITE | S_IRUSR | S_IWUSR);
    if (cf == -1)
        printf("Error in opening file");
    else
        printf("file opened successfully");
    close(cf);
    of = open(fName, O_CREAT, O_RDWR, O_APPEND, 0666);
    printf("%d", of);
    printf("ENTER ANYTHING TO WRITE :: ");
    scanf("%s", fData);
    write(of, "q", 1);
    close(of);
    return 0;
}