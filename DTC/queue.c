#include<stdio.h>
#define MAX 50

int queue[MAX];
int front = -1, rear = -1;


void enqueue(){

    if(rear == MAX)
        printf("Stack Full !!");
    else{
        int temp;
        printf("Enter the data :: ");
        scanf("%d", &temp);
        rear++;
        queue[rear]=temp;
        if(front==-1) 
            front=0;
    }
    
}

void dequeue(){
    if(front == -1)
        printf("Stack Empty !!");
    else{
        printf("Popped Element :: %d",queue[front]);
        front++;
    }
}

void display(){
    int i;
    if(front != -1)
    {
        for(i=rear;i>=front;i--){
            printf("%d --> ",queue[i]);
        }
    }
    else
        printf("Queue Empty");
    
}

int main(){

    enqueue();
    enqueue();
    enqueue();
    enqueue();
    dequeue();
    display();


    return 0;
}