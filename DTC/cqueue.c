#include <stdio.h>
#define MAX_SIZE 20

int data[MAX_SIZE];
int front = -1, rear = -1;

int isqEmpty()
{
    if (front == -1 && rear == -1)
        return 1;
    else
        return 0;
}

int isqFull()
{
    if ((rear + 1) % MAX_SIZE == front)
        return 1;
    else
        return 0;
}

void enqueue()
{
    int n;
    if (isqFull())
    {
        printf("\nQueue is Full!!!");
    }
    else if (isqEmpty())
    {
        printf("\n Enter the data to enter ::: ");
        scanf("%d", &n);
        rear = front = 0;
        data[rear] = n;
    }
    else
    {
        printf("\n Enter the data to enter ::: ");
        scanf("%d", &n);
        rear = (rear + 1) % MAX_SIZE;
        data[rear] = n;
    }
}

void dequeue()
{
    if (isqEmpty())
    {
        printf("\nQueue is Empty!!!");
    }
    else if (front == rear)
    {
        printf("\nElement Dequeued :: %d", data[front]);
        front = rear = -1;
    }
    else
    {
        printf("\nElement Dequeued :: %d", data[front]);
        front = (front + 1) % MAX_SIZE;
    }
}

void display()
{
    if (!isqEmpty())
    {
        int i = rear;
        for (; i != front; i = (i - 1 + MAX_SIZE) % MAX_SIZE)
        {
            printf("%d --> ", data[i]);
        }
        printf("%d --> ", data[i]);
    }
    else
        printf("Queue is Empty !!");
}

int main()
{
    int ch;
    while (1)
    {
        printf("\n1.Enqueue\n2.Dequeue\n3.Display Queue\n0.EXIT");
        printf("\n Enter your choice ::: ");
        scanf("%d", &ch);
        switch (ch)
        {
        case 1:
            enqueue();
            break;
        case 2:
            dequeue();
            break;
        case 3:
            display();
            break;
        case 0:
            exit(0);
        default:
            printf("Wrong Choice !!");
            break;
        }
    }

    return 0;
}