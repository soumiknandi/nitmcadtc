#include <stdio.h>
#define MAX 100

int main(int argc, char const *argv[])
{
    int data[MAX];
    int n, i, j, k;
    printf("Enter the size of n (MAX 100) : ");
    scanf("%d", &n);

    printf("\nENTER THE DATA");
    for (i = 0; i < n; i++)
    {
        printf("POSITION %d :: ", i + 1);
        scanf("%d", &data[i]);
    }

    printf("\nThe Unsorted List is : ");
    for (k = 0; k < n; k++)
    {
        printf("%d, ", data[k]);
    }

    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n - i - 1; j++)
        {
            if (data[j] > data[j + 1])
            {
                int temp = data[j];
                data[j] = data[j + 1];
                data[j + 1] = temp;
            }
        }
        /*
        // delete the above line and ....

        printf("\nAfter %d Iteration : ",i+1);
        for(k=0;k<n;k++){
            printf("%d, ",data[k]);
        }

        // .... delete the below line the get detailed view        
        */
    }

    printf("\nFinal Sort Result : ");

    for (k = 0; k < n; k++)
    {
        printf("%d, ", data[k]);
    }

    return 0;
}
