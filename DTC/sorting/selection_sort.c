#include<stdio.h>
#define MAX 200

int main(int argc, char const *argv[])
{
    int data[MAX];
    int i,j,k,n;
    printf("Enter the no of elements(MAX 200) : ");
    scanf("%d",&n);

    printf("Enter the elements \n");
    for(i=0;i<n;i++){
        printf("Position %d :: ",i + 1);
        scanf("%d",&data[i]);
    }
    printf("\nThe Unsorted List is : ");
    for(k=0;k<n;k++){
        printf("%d, ",data[k]);
    }

    for(i=0;i<n-1;i++){
        int min = i;
        for(j=i+1;j<n;j++){
            if(data[j] < data[min] )
            min = j;
        }
        int temp = data[i];
        data[i] = data[min];
        data[min] = temp;
        /*
        // delete the above line and ....

        printf("\nSwapping %d at position %d and %d at position %d ",data[i],min+1,data[min],i+1);
        printf("\nAfter Swaping : ");
        for(k=0;k<n;k++){
            printf("%d, ",data[k]);
        }

        // .... delete the below line the get detailed view        
        */
        
    }


    printf("\nThe Sorted List Is : ");
    for(i=0;i<n;i++){
        printf("%d, ",data[i]);
    }

    return 0;
}

