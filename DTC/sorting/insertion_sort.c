#include<stdio.h>
#define MAX 50
int main(int argc, char const *argv[])
{
    int data[MAX];
    int n,i,hole;
    printf("Enter the no of elements(MAX 50) : ");
    scanf("%d",&n);

    printf("Enter the values \n");
    for(i=0;i<n;i++){
        printf("Position %d :: ", i+1);
        scanf("%d",&data[i]);
    }
    /*
    * we assume first element is sorted
    * we compare the 2nd element value 
    * with the first element
    * if it smaller we push/shift the 
    * first element one position to right
    * and place the 2nd element in the first pos
    */
    for(i=1;i<n;i++){
        int value = data[i];
        hole = i;
        while(hole > 0 && data[hole-1]>value){
            data[hole] = data[hole-1];
            hole--;
        }
        data[hole] = value;

    }
    printf("Full List : ");
    for(i=0;i<n;i++){
        printf("%d, ", data[i]);
    }
    return 0;
}
