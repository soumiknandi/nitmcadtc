#include<stdio.h>
#include<stdlib.h>


struct node {
    int data;
    struct node *next;
};
// Global Declarations
struct node *head = NULL;


void clear(){
    #if defined(__linux__) || defined(__unix__) || defined(__APPLE__)
        system("clear");
    #endif

    #if defined(_WIN32) || defined(_WIN64)
        system("cls");
    #endif
}


//Get Data From User
int getData(){
    int n;
    printf("\nEnter the value to insert : ");
    scanf("%d",&n);
    return n;
}


void insertAtHead(){

    //Temp node to store data
    struct node *temp = (struct node*)malloc(sizeof(struct node));

    //If no list is present
    if(head == NULL){
        temp -> data = getData();
        temp -> next = NULL;
        head = temp;
    } else{
        temp -> data = getData();
        temp -> next = head;
        head = temp;
    }
}

void insertAtTail(){
    if(head == NULL)
        insertAtHead();
    else{
        struct node *lastNode = head;

        while(lastNode->next != NULL){
            lastNode = lastNode -> next;
        }

        struct node *temp = (struct node*)malloc(sizeof(struct node));
        temp -> data = getData();
        temp -> next = NULL;
        lastNode -> next = temp;

    }
}

void insertAtPOSAfter(){
    if(head == NULL){
        printf("\nList Is Empty Inserting At Head");
        insertAtHead();
    } else{
        int pos,tempPos = 0;
        printf("Enter The Position Where It Should Be Inserted : ");
        scanf("%d",&pos);

        struct node *lastNode = head;
        while(lastNode->next != NULL && (pos - 1) > tempPos){
            tempPos++;
            lastNode = lastNode -> next;
        }
        
        if(pos - 1 == tempPos){
            struct node *temp = (struct node*)malloc(sizeof(struct node));
            temp -> data = getData();
            temp -> next = lastNode -> next;
            lastNode -> next = temp;
        }

    }
}

void insertAtPOSBefore(){
    if(head == NULL){
        printf("\nList Is Empty Inserting At Head");
        insertAtHead();
    } else{
        int pos,tempPos = 0;
        printf("Enter The Position Where It Should Be Inserted : ");
        scanf("%d",&pos);

        struct node *lastNode = head;
        while(lastNode->next != NULL && (pos - 2) > tempPos){
            tempPos++;
            lastNode = lastNode -> next;
        }
        
        if(pos - 2 == tempPos){
            struct node *temp = (struct node*)malloc(sizeof(struct node));
            temp -> data = getData();
            temp -> next = lastNode -> next;
            lastNode -> next = temp;
        }

    }
}

void displayList(){
    if(head == NULL)
        printf("\nLinked List Is Empty");
    else{
        struct node *lastNode = head;
        while(lastNode -> next != NULL){
            printf("%d => ",lastNode -> data);
            lastNode = lastNode -> next;
        }
        printf("%d",lastNode -> data);

    }
}

void delecteItemUsingPOS(){
    int pos;
    printf("\nEnter the position the delete : ");
    scanf("%d",&pos);

    if(pos <= 0){
        printf("INVALID POSITION!!");
        return;
    }else if(head == NULL){
        printf("Linked List Is EMPTY");
        return;
    }else{
        int i;
        struct node *present = head;
        struct node *previous = NULL;

        //Iff first item
        if(pos == 1){
            head = head -> next;
            printf("\nItem Deleted : %d",present->data);
            free(present);
            return;
        }

        //Iterate To POS
        for(i=0;i<pos-1;i++){
            if(present -> next != NULL){
                previous = present;
                present = present->next;
            }
        }

        //Iff item is in middle
        if(present -> next != NULL){
            previous -> next = present->next;
            printf("\nItem Deleted : %d",present->data);
            free(present);
        //Iff item is at last
        }else{ 
            previous -> next = NULL;
            printf("\nItem Deleted : %d",present->data);
            free(present);
        }
    }
}

void reversell(){
    struct node *previous = NULL;
    struct node *current = head;
    struct node *next = NULL;

    if(current==NULL)
        printf("NOT ");
    else{
        while (current != NULL){
            next = current->next;
            current->next = previous;
            previous = current;
            current = next;        
        }
        head = previous;
    }
}


int main(int argc, char const *argv[])
{
    int ch;
    while(1){
        printf("\nMENU\n");
        printf("\n1.Insert At Head");
        printf("\n2.Insert At Tail");
        printf("\n3.Delete Item By Position");
        printf("\n4.Insert Item After Position");
        printf("\n5.Insert Item Before Position");
        printf("\n9.Display");
        printf("\n0.EXIT");
        printf("\nEnter your choice : ");
        scanf("%d",&ch);
        int temp;
        switch(ch){
            case 0 :return 0;
            case 1 :insertAtHead();
                    break;
            case 2 :insertAtTail();
                    break;
            case 3 :delecteItemUsingPOS();
                    break;
            case 4 :insertAtPOSAfter();
                    break;
            case 5 :insertAtPOSBefore();
                    break;
            case 9 :displayList();
                    break;
            case 10:reversell();
                    break;
            default:printf("Wrong Option!");
        }
    }

    return 0;
}
