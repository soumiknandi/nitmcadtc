#include <stdio.h>
#define MAX_SIZE 5

int data[MAX_SIZE];
int top = -1;

void push(int n)
{
    top++;
    data[top] = n;
}

void pop()
{
    if (isEmpty())
    {
        printf("\nPopped element :: %d", data[top]);
        top--;
    }
    else
        printf("Stack Already Empty");
}
void getData()
{
    int n;
    printf("Enter the value to enter : ");
    scanf("%d", &n);
    if (isFull())
        push(n);
    else
        printf("Stack Full!!!");
}

void display()
{
    int i;
    for (i = 0; i <= top; i++)
    {
        printf("\n----\n%d", data[i]);
    }
}

int isFull()
{
    if (top >= MAX_SIZE - 1)
        return 0;
    else
        return 1;
}

int isEmpty()
{
    if (top == -1)
        return 0;
    else
        return 1;
}

int main()
{
    int n;
    while (1)
    {
        printf("\n\n1.PUSH'\n2.POP\n3.Display\n0.EXIT");
        printf("\nEnter Your Choice ::: ");
        scanf("%d", &n);
        switch (n)
        {
        case 0:
            exit(0);
            break;
        case 1:
            getData();
            break;
        case 2:
            pop();
            break;
        case 3:
            display();
            break;
        default:
            printf("Wrong Options1!");
            break;
        }
    }
    return 0;
}