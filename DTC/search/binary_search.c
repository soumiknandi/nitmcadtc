#include <stdio.h>
#define MAX 50
int main(int argc, char const *argv[])
{
    int data[MAX];
    int n, i, j, k, search;

    printf("Enter the value of n (MAX:50) : ");
    scanf("%d", &n);

    if (n <= 50)
    {
        printf("\nEnter the value of each positions ");
        for (i = 0; i < n; i++)
        {
            printf("Position %d ::  ", i + 1);
            scanf("%d", &data[i]);
        }

        for (j = 0; j < n; j++)
        {
            for (k = 0; k < n - 1; k++)
            {
                if (data[k] > data[k + 1])
                {
                    int temp = data[k];
                    data[k] = data[k + 1];
                    data[k + 1] = temp;
                }
            }
        }

        for (i = 0; i < n; i++)
        {
            printf("%d ,", data[i]);
        }

        printf("Enter the value to search : ");
        scanf("%d", &search);
        int low = 0, high = n;

        while (low <= high)
        {
            int mid = low + (high - low) / 2;

            if (data[mid] == search)
            {
                printf("Item Found At Position %d ", mid + 1);
                return 0;
            }
            else if (search > data[mid])
            {
                low = mid + 1;
            }
            else if (search < data[mid])
            {
                high = mid - 1;
            }
        }

        printf("Item Not Found");
    }
    else
    {
        printf("Too Many Element!!");
    }

    return 0;
}
