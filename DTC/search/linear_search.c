#include <stdio.h>
#define MAX 50

int main()
{
    int data[MAX];
    int n, i, find;
    printf("Enter the max size (MAX 50) : ");
    scanf("%d", &n);

    printf("\nENTER THE DATA");
    for (i = 0; i < n; i++)
    {
        printf("\nPOSITION %d :: ", i + 1);
        scanf("%d", &data[i]);
    }

    printf("\n Enter the value to find : ");
    scanf("%d", &find);

    for (i = 0; i < n; i++)
    {
        if (data[i] == find)
        {
            printf("\nValue found at position %d", i + 1);
            return 0;
        }
    }
    printf("\nUnable to find the given value");

    return 0;
}