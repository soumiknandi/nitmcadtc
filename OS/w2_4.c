#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
int main()
{
    int p;
    p = fork();
    if (p == 0)
    {
        printf("\n Child created ");
        exit(0);
        printf("\n Process ended ");
    }
    if (p < 0)
    {
        printf("\n Cannot create child ");
        exit(-1);
        printf("\n Process ended ");
    }
    return 0;
}