/* 5. Program using exit() and wait() system call */
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
int main()
{
    unsigned int status;
    if (fork() == 0) /* == 0 means in child */
    {
        scanf("%d", &status);
        exit(status);
    }
    else /* != 0 means in parent */
    {
        wait(&status);
        printf("child exit status = %d\n", status > 8);
    }
    return 0;
}