/* 2. Program using fork() system call */
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
int main()
{
    printf("\n This is to demonstrate the fork()");
    fork();
    printf("\nAfter fork()...PID=%d", getpid());
    return 0;
}