/* 3. Using - open() ,read() and write() system calls */
/* reading a content from file and displaying it to monitor */
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
int main()
{
    char buf[100], fn[10];
    int fd, n;
    printf("Enter file name: ");
    scanf("%s", fn);
    fd = open(fn, O_RDONLY);
    n = read(fd, buf, 100);
    n = write(1, buf, n); //write to monitor
    close(fd);
    return 0;
}