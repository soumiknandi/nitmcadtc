/* 2. using open(), write() system call */
#include <fcntl.h>
#include <unistd.h>
void main()
{
    int fd, i;
    fd = open("test", O_CREAT | O_RDWR | O_APPEND, 0666);
    for (i = 0; i < 100; i++)
        write(fd, "A", 1);
    close(fd);
}