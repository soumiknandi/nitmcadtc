/* 3. fork() and pid */
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
int main()
{
    int pid;
    pid = fork();
    if (pid == 0)
    {
        printf("\n I am the child, my process ID is %d ", getpid());
        printf("\n I am the child's parent process ID is %d ", getppid());
    }
    else
    {
        printf("\n I am the parent, my process ID is %d ", getpid());
        printf("\n I am the parent's parent process ID is %d ", getppid());
    }
    return 0;
}