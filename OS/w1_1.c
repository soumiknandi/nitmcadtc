/* 1. Program to print the process id */

#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
int main()
{
    int pid, ppid;
    pid = getpid();
    ppid = getppid();
    printf("\n Process Id is %d\n", pid);
    printf("\n Parent Process Id is %d\n", ppid);
    return 0;
}