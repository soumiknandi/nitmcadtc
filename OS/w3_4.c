/* 4. using - write() system call writing line of text to the file */
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
int main()
{
    char buf[100], fn[10];
    int fd, n;
    printf("enter file name:");
    scanf("%s", fn);
    printf("\n enter text\n");
    scanf("%s", buf);
    fd = open(fn, O_CREAT | O_WRONLY);
    n = write(fd, buf, strlen(buf));
    close(fd);
    return 0;
}
