/* 5. using - open() ,read() and write() system calls: file copy operation*/
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
int main()
{
    char buf[1000], fn1[10], fn2[10];
    int fd1, fd2, n;
    printf("Enter source file name: ");
    scanf("%s", fn1);
    printf("Enter destination file name: ");
    scanf("%s", fn2);
    fd1 = open(fn1, O_RDONLY);
    n = read(fd1, buf, 1000);                  //read from source file
    fd2 = open(fn2, O_CREAT | O_WRONLY, 0666); //create the destn file in Read, Write mode
    n = write(fd2, buf, n);                    //write to destn file
    close(fd1);
    close(fd2);
    return 0;
}
