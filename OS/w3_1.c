/* 1. using creat() system call */
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
/* sys/stat.h defines S_IREAD & S_IWRITE */
int main()
{
    int fd;

    fd = creat("datafile.dat", __S_IREAD | __S_IWRITE);
    if (fd == -1)
        printf("Error in opening datafile.dat\n");
    else
    {
        printf("datafile.dat opened for read/write access\n");
        printf("datafile.dat is currently empty\n");
    }
    close(fd);
    exit(0);
}