#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
main()
{
    int pid, i = 0;
    printf("\n Ready to fork");
    pid = fork();
    if (pid == 0)
    {
        printf("\n Child starts ");
        for (i = 0; i < 1000; i++);
        printf("\n Child ends ");
    }
    else
    {
        wait(0);
        for (i = 0; i < 1000; i++);
        printf("\n Parent process ends ");

    }
}
