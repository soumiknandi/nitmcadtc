/* 7. Program using opendir() and readdir() system call */
#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
int main(int argc, char *argv[])
{
    DIR *dp;
    struct dirent *dirp;
    if (argc != 2)
    {
        printf("a single argument (the directory name) is required\n");
        exit(1);
    }
    if ((dp = opendir(argv[1])) == NULL)
    {
        printf("can't open %s\n", argv[1]);
        exit(1);
    }
    while ((dirp = readdir(dp)) != NULL)
        printf("%s %d\n", dirp->d_name, dirp->d_ino);
    closedir(dp);
    exit(0);
}